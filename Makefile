ASM=nasm
ASMFLAGS=-f elf64
LD=ld

start: clean program

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o dict.o lib.o
	$(LD) -o $@ $^
	./program

clean:
	rm -f *.o

.PHONY: start clean
