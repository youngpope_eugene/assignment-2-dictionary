section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60
    xor  rdi, rdi          
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1  
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1   
    mov rdi, 1          
    mov rsi, rsp  
    mov rdx, 1          
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, 10    
	mov rax, rdi	    
	xor rcx, rcx		    
	
	dec rsp			
	inc rcx
	mov byte [rsp], 0
.loop:
	xor rdx, rdx     
	div r9         
	add rdx, '0'        
	dec rsp
	inc rcx
	mov byte [rsp], dl   	
	test rax, rax       	
	jnz .loop		       
	
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor rdx, rdx
	cmp rdi, 0		    
	jns .print_unsign	
	push rdi		   
	mov rdi, '-'
	call print_char		
	pop rdi			
	neg rdi			   
    .print_unsign:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  push rbx
  xor rbx, rbx
    xor rax, rax
  .loop:
    mov cl, byte [rdi+rbx]
    cmp cl, byte [rsi+rbx]
    jnz .not_ok
    cmp byte [rdi + rbx], 0
    jz .ok
    inc rbx
    jmp .loop
  .ok:
    mov rax, 1
    jmp .end
  .not_ok:
    mov rax, 0
    jmp .end
  .end:
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0			
	mov rdx, 1		
	mov rsi, rsp	
	mov rdi, 0  	
	xor rax, rax	
	syscall
	pop rax			
 	ret 
    

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    .read_spaces:
     call read_char
        cmp al, 0x20
        je .read_spaces
        cmp al, 0x9
        je .read_spaces
        cmp al, 0xA
        je .read_spaces
    xor rdx, rdx
    .loop:
        cmp al, 0xA
        je .finish
        cmp al, 0x20
        je .finish
        cmp al, 4
        je .finish
        cmp al, 0x9
        je .finish
        cmp al, 0
        je .finish
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        push rdx
        push r8
        call read_char
        pop r8
        pop rdx
        jmp .loop
    .finish:
        mov byte [r8+rdx], 0
        mov rax, r8
        ret
    .overflow:
        xor rax, rax
        ret


; Принимает указатель на строку, пытается (rdi)
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor r8, r8
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi
    mov r9, 10
    .loop:
        mov sil, [rdi+r8]
        cmp sil, '0'
        jl .finish
        cmp sil, '9'
        jg .finish
        inc r8
        sub sil, '0'
        mul r9
        add rax, rsi
        jmp .loop
    .finish:
        mov rdx, r8
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	cmp byte [rdi], '-'	
	je .ngt	
	call parse_uint		
	ret
    .ngt:
        inc rdi
        call parse_uint		
        neg rax			
        inc rdx			
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
	push rdi
	push rsi
	push rdx		    
	call string_length	
	pop rdx
	pop rsi
	pop rdi			    
	inc rax			    
	cmp rax, rdx		
	ja .finish		
	xor rcx, rcx		
    .loop:
        mov dl, byte [rdi]	
        mov byte [rsi], dl	
        inc rdi
        inc rsi			    
        inc rcx		    	
        cmp rcx, rax		
        jna .loop		    
        dec rax			    
        ret
    .finish:
        mov rax, 0		    
        ret  
