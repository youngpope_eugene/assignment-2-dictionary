%define START_INDEX 0 
%macro colon 2
	%ifid %2
	%else
		%error "Error: name of object"
	%endif
	%ifstr %1
	%else
		%error "Error: value of object"
	%endif
	%2:
	dq START_INDEX
	db %1, 0
%define START_INDEX %2
%endmacro
