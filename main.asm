%include "lib.inc"
%include 'colon.inc'
%define buffer_size 256

section .rodata
%include 'words.inc'
enter_message: db "Enter key: ", 0
fail_message: db "Wrong key", 0
too_long_message: db "Key is too long", 0
value_message: db "Got value: ", 0

section .bss
buffer: resb buffer_size

section .text

global _start
extern find_word

_start: 
        mov rdi, enter_message
        call print_string
        mov rdi, buffer
        mov rsi, buffer_size
        call read_word
        test rax, rax
        je .too_long_issue
        push rdx 
        mov rdi, buffer 
        mov rsi, START_INDEX 
        call find_word 
        test rax, rax
        je .fail
        pop rdx
        add rax, rdx
        inc rax

    .success:
        mov rdi, value_message
        call print_string
        pop rdi
        call print_string 
        call print_newline
        mov rdi, 0
        jmp exit

    .fail:
        mov rdi, fail_message
        call print_string
        mov rdi, 0
        jmp exit

    .too_long_issue: 
        mov rdi, too_long_message
        call print_string
        mov rdi, 0
        jmp exit
