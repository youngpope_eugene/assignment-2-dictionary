%include "lib.inc"
%define eight 8

section .text

global find_word

find_word:
    test rsi, rsi
    je .fail
    add rsi, eight
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    jz .success
    sub rsi, eight
    mov rsi, [rsi]
    jmp find_word

    .fail:
        mov rax, 0
        ret

    .success:
        mov rax, rsi
        ret
